# Hong Kong Backup Forum Documentation

This repository contains the architecture design, introduction and other details of this project.

## Motivation

At the time of writing of this document (July 2019), it is observable that the freedom of speech granted to Hong Kong citizens will be slowly worsening, due to the political situations affected by the Anti-Extradition Law movement. Specifically, since this movement is organized in a decentralized way, the LIHKG forum, which is a well-operated forum with most of its members do not agree with the bill, may seek heavy attack or even more aggressive actions from the Government. Therefore, this projects aims to build a drop-in replacement of the forum and prepares it to be immediately usable if the LIHKG forum is compromised. 

This project **does not seeks to be a competitor with the major forums of Hong Kong**, instead, it should be considered as a backup effort of it. Also, since all code in this project is licensed under the MIT license, operators of forums from all around the world may take the code here to use it in their own forums, and may also contribute back their changes as well.

## Design Goals

At this era of the internet, pretty much everything is cloud-based. Therefore, this project is designed in a cloud-native, microservices architecture. Considering that the growing number of current users of the LIHKG forum which already exceed two hundred thousands with more than one million posts and an even larger amount of comments, our architecture must also be designed to be highly scalable, ideally auto-scalable. Therefore, we made some backend design choices as noted in ARCHITECTURE.md.

Also, we would like to make sure that it is possible for others to contribute new clients to the community as they like. Therefore, we decided to stabilize the API specification. One may obtain a version of such specification by looking at https://gitlab.com/hkbackupforum/apispec/.

## Action Plan

The core of any forum platform is its users rather than its programming. Therefore, we will need a rigid action plan prior to the activation of this project. The initial plan here is, after we noticed that the LIHKG forum is compromised, we can seek help from Apple Daily and Stand News, which are considered as trusted media in Hong Kong, to provide some promotion of this app, as well act as an independent party to monitor that we do protect the privacy of the users. We may also start an NGO to cover the operation of such a forum as well and receive donations and subscriptions from our users to cover operation and development costs.