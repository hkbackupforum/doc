# Architecture

## Goals

- Scalability  
  In the best situation, this project should only have to handle a small amount of traffic, mainly from its developers and test users. However, in a foreseeable situation where this project is used to serve as a replacement of the major forums in Hong Kong to protect our freedom of speech, it will receive a very large amount of traffic, including those valid ones from real users and invalid ones from DDOS attackers and abusive users. Therefore, this project must be designed with scalability in mind.

- High Availability  
  The recent incident that LIHKG is unavailable to users is considered as very bad user experience, even considered as horrible as some believed that that is because of an attack from China Government. Although it turns out that that is just the problem of Cloudfare, it still reflects how important does the users value such a platform.

- Compatibility  
  As an open-source project, it is foreseeable that we may have multiple implementations of both of the frontend apps and backend infrastructure. Therefore, it is important for us to define a suite of stable external APIs for consumptions so that multiple versions can be compatible.

## Design choices

- Immutability
  To reduce complexity, the content here is designed to be uneditable after they have been posted. This will help easier cache management. However, it is still possible to remove content if requested by court/users and related tools will be provided to ease the process.

- Signed and validated resource identifiders
  To reduce the effect of L7 DDOS attacks on our service, we would like to make the resource identifiders JWTs that can be validated to prevent attack based on cahce miss.

## Backend infrastructure

### Stateful services

- forum-write-queue  
  The write-queue will serve as a buffer zone between the external APIs that involves write operations and the database. All validated write operations from authenticated users, such as adding posts and comments, will be queued here and being consumed by workers to be written into the database. Its existence is to make sure that sudden spikes in write usage will not result in overloading so that the database service can take time to scale up if applicable while offering limited service. It seems that RabbitMQ is a good option here.

- forum-db  
  The forum-db will serve as the data storage for all forum posts and comments. It has to be fault-tolarent, and can be scaled indefinitely. It has to withstand very high traffic. Ideally it also needs to be SQL-comptatible. It seems that CockroachDB with PostgreSQL compatibility could be a nice option here.

- forum-search  
  The forum-search will serve as the text search backend for forum contents. It has to be scalable. Elasticsearch seems to be a nice option here.

- forum-cache  
  The forum-cache will serve as the caching layer for read operations so that the loading of database can be reduced. Redis with on disk persistence maybe the best choice for this.

- user-db  
  The user-db will serve as the data storage for sensetive data of users, such as their emails, logins and salted password hashes. It will be seperated from the main forum db for security and scalbility concerns. CockroachDB seems to be a nice choice here.

### Stateless services

- forum-write-api  
  The forum-write-api will act as the first service that any client-side forum write operations go through. It will expose a gRPC service that allows users to create posts and add comments. Once it receive the write requests, it will insert it into the forum-white-queue and start to count for a timeout. If the queue returns that the corresponding content is added before timeout, it will respond to the user saying the post is already added. If the queue cannot return on time, it will respond to the user saying the post is already and queued for process. The message validation and user verifyingtasks should be done here. Until Cloudfare provides gRPC support, this service should not be exposed to prevent DDOS attacks.

- forum-write-worker  
  The forum-write-worker will act as a worker that do the real write operations on forum-db. It will keep watching forum-write-queue for new contents, and take a request from the queue if there are unresolved requests. It will then try to add the content into forum-db. If the operation is successful, it will acknowledge the queue for success. If the operation is failed, it will acknowledge the queue for failure so that the content can be processed later. This service should not be exposed to public.

- forum-read-api  
  The forum-read-api will act as the first service that any client-side forum read operations go through. It will expose a gRPC service that allows users to read posts and comments. Once it receive the read requests, it immediately check whether the forum-cache whether such cache exist. If the cache hit then the cached results will be returned to the user. If there are a cache miss, then the forum-db will be queried and then such data will be returned to the user. A new cache for the respective content should also be created concurrently. Until Cloudfare provides gRPC support, this service should not be exposed to prevent DDOS attacks.

- forum-read-auth-api  
  The forum-read-auth-api does things quite same as forum-read-api, except that it (1) check for a valid jwt, (2) provide contents that are hidden to unauthorized users and (3) will log user actions to prevent abuse.

- user-auth-api  
  The user-auth-api will provide authentication interface for registered users so that they can retrieve a jwt they can be used for service that is only for logined users. It will also provides an interface for registration.
